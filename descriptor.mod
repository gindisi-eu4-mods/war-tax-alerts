name="War Tax Alerts"
version="1.0.2"
picture="thumbnail.png"
dependencies={
	"Anbennar: A Fantasy Total Conversion"
	"Anbennar-PublicFork"
	"Anbennar"
	"Anbennar MP modified"
}
tags={
	"Gameplay"
	"Fixes"
}
supported_version="v1.37.*.*"
remote_file_id="3074779299"